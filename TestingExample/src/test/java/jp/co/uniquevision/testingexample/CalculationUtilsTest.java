package jp.co.uniquevision.testingexample;

import org.junit.Test;
import jp.co.uniquevision.testingexample.CalculationUtils;

import static org.fest.assertions.api.Assertions.assertThat;

public class CalculationUtilsTest {
    @Test public void shouldAddLhsAndRhs() {
        assertThat(CalculationUtils.plus(5, 1)).isEqualTo(6);
    }
}