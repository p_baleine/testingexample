package jp.co.uniquevision.testingexample;

import android.widget.EditText;
import android.widget.TextView;
import java.lang.Exception;
import jp.co.uniquevision.testingexample.MainActivity;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;

import static org.fest.assertions.api.Assertions.assertThat;

@RunWith(RobolectricGradleTestRunner.class)
public class MainActivityTest {
    private MainActivity mActivity;
    private EditText mLhsEditText;
    private EditText mRhsEditText;
    private EditText mResultEditText;

    @Before public void setUp() throws Exception {
        mActivity = Robolectric.buildActivity(MainActivity.class).create().get();
        mLhsEditText = (EditText) mActivity.findViewById(R.id.lhs);
        mRhsEditText = (EditText) mActivity.findViewById(R.id.rhs);
    }

    @Test public void shouldSetResultFieldWithLhsPlusRhs() {
        mLhsEditText.setText("1");
        mRhsEditText.setText("2");

        assertThat("3").isEqualTo("3");
    }
}