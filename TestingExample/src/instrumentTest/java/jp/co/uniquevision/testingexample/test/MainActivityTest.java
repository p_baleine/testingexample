package jp.co.uniquevision.testingexample.test;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Instrumentation;
import android.os.Build;
import android.test.ActivityInstrumentationTestCase2;
import android.widget.EditText;
import android.widget.TextView;

import jp.co.uniquevision.testingexample.MainActivity;
import jp.co.uniquevision.testingexample.R;

import static android.test.ViewAsserts.assertBottomAligned;
import static android.test.ViewAsserts.assertOnScreen;

/**
 * Created by tajima-junpei on 13/12/05.
 */
public class MainActivityTest extends ActivityInstrumentationTestCase2<MainActivity> {
    private Instrumentation mInstrumentation;
    private Activity mMainActivity;
    private EditText mLhsInput;
    private EditText mRhsInput;
    private TextView mResult;

    @TargetApi(Build.VERSION_CODES.FROYO)
    public MainActivityTest() {
        super(MainActivity.class);
    }

    protected void setUp() throws Exception {
        super.setUp();

        mInstrumentation = getInstrumentation();
        mMainActivity = getActivity();
        mLhsInput = (EditText) mMainActivity.findViewById(R.id.lhs);
        mRhsInput = (EditText) mMainActivity.findViewById(R.id.rhs);
        mResult = (TextView) mMainActivity.findViewById(R.id.result);
    }

    public void testEditTextsAndTextViewAreOnScreen() {
        assertOnScreen(mMainActivity.getWindow().getDecorView(), mLhsInput);
        assertOnScreen(mMainActivity.getWindow().getDecorView(), mRhsInput);
        assertOnScreen(mMainActivity.getWindow().getDecorView(), mResult);
    }

    public void testCalculation() {
        mMainActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mLhsInput.setText("123");
                mRhsInput.setText("456");
            }
        });

        mInstrumentation.waitForIdleSync();

        assertEquals("579", mResult.getText().toString());
    }
}
